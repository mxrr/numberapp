#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <string>
#include <conio.h>
#include "methods.h"

using namespace std;



//Init error code
int errorCode = 0;

int main()
{
    cout << "/////////////////////////////////////////" << endl
    << "/////////         v1.01          /////////" << endl
    << "/////////////////////////////////////////" << endl
    << "Use 'edit' to edit values file " << endl
    << "Type a number to load the value of that row " << endl
    << "Use 'exit' to exit the program " << endl;
    //Check if there is a error code and print it, then return it to 0 and run the program again
    if(errorCode != 0){
        cout << "$ err: " << errorCode << endl;
        errorCode = 0;
    }

    //Get user input
    string usrInput;
    cout << "$ ";
    cin >> usrInput;


    //Check if the input contains numbers and communicate that to the check method
    if(usrInput.find_first_of("0123456789") != std::string::npos){
        chkInput(stoi(usrInput), usrInput, 1);
    }//If the input has no numbers, only send the string value
    else{
        //Check that there is no error code and proceed to run the program
        if(errorCode == 0){
            chkInput(0, usrInput, 0);
        }//Check if problem was not catched during program and output unknown error
        else{
            errorCode = 512;
            cout << "$ Unknown error: " << errorCode << endl;
        }
    }


    return errorCode;
}



string loadKey(int valueRow = 0)
{


    //Init variables and inputStream
    ifstream inputStream("values.txt");
    string outputStr[300];
    int currentRow = 0;

    //DEBUG
    //cout << valueRow << endl;




    //Open values.txt and load the file into an array
    if(inputStream.is_open()){
        while(currentRow <= valueRow){
            getline(inputStream, outputStr[currentRow]);
            currentRow++;
        }
        //Close the file if it's open but not doing anything
        inputStream.close();
    }

    //Return the key value into main.
    return outputStr[currentRow - 1];
}



void chkInput(int usrInputChkInt = 0, string usrInputChkStr = " ", int chkSelector = 0)
{
    /*DEBUG
    cout << "String: " << usrInputChkStr << endl;
    cout << "Int: " << usrInputChkInt << endl;


    //DEBUG*/


    //If selector = 0, input only contains a string; so load commands based on input
    if(chkSelector == 0){
        //Load exit command on input
        if(usrInputChkStr == "exit"){
            exit(0);
        }//Load edit command on input
        if(usrInputChkStr == "edit"){
            cout << "$ Editing file" << endl;
            system("notepad \"values\"");
            clear();
            main();
        }

    }//If selector = 1, input contains numbers.
    if(chkSelector == 1){
        //Check for invalid inputs
        if(usrInputChkInt <= 0){
            cout << "$ Invalid value " << endl;
            errorCode = 100;
            clear();
            main();
        }
        if(usrInputChkInt >= 301){
            cout << "$ Invalid value " << endl;
            errorCode = 101;
            clear();
            main();
        }
        //load the key on user input
        else{
            cout << "$ KEY: " << loadKey(usrInputChkInt - 1) << endl << "$ Press any key to return...";
            getch();
            clear();
            main();
        }
    }
    else{
        cout << "$ Value error" << endl;
        errorCode = 404;
        clear();
        main();
    }
}


